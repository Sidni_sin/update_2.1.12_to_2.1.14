<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magento\Integration\Controller\Token;

class Request extends \Magento\Framework\App\Action\Action
{
    /**
     * @var  \Magento\Framework\Oauth\OauthInterface
     */
    protected $oauthService;

    /**
     * @var  \Magento\Framework\Oauth\Helper\Request
     */
    protected $helper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Oauth\OauthInterface $oauthService
     * @param \Magento\Framework\Oauth\Helper\Request $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Oauth\OauthInterface $oauthService,
        \Magento\Framework\Oauth\Helper\Request $helper
    ) {
        parent::__construct($context);
        $this->oauthService = $oauthService;
        $this->helper = $helper;
    }

    /**
     *  Initiate RequestToken request operation
     *
     * @return void
     */
    public function execute()
    {
        $writer = new \Zend\Log\Writer\Stream('/var/www/html/sunuva/current/var/log/oauth_request.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        try {
            $requestUrl = $this->helper->getRequestUrl($this->getRequest());
            $request = $this->helper->prepareRequest($this->getRequest(), $requestUrl);
            $logger->info('request url: '.$requestUrl);
            $logger->info('request params: '.print_r($request, true));
            // Request request token
            $response = $this->oauthService->getRequestToken($request, $requestUrl, $this->getRequest()->getMethod());
        } catch (\Exception $exception) {
            $response = $this->helper->prepareErrorResponse($exception, $this->getResponse());
            $logger->debug('Error: '.$exception->getMessage());
        }
        $this->getResponse()->setBody(http_build_query($response));
    }
}
